package se325.lab01.concert.common;

import java.time.LocalDateTime;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


/**
 * Class to represent a music concert.
 */
public class ConcertServant extends UnicastRemoteObject implements Concert {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String title;
    private LocalDateTime date;

    public ConcertServant(Long id, String title, LocalDateTime date) throws RemoteException {
        this.id = id;
        this.title = title;
        this.date = date;
    }

    public ConcertServant(String title, LocalDateTime date) throws RemoteException {
        this(null, title, date);
    }

    public Long getId() throws RemoteException {
        return id;
    }

    public void setId(Long id) throws RemoteException {
        this.id = id;
    }

    public String getTitle() throws RemoteException {
        return title;
    }

    public void setTitle(String title) throws RemoteException {
        this.title = title;
    }

    public LocalDateTime getDate() throws RemoteException {
        return date;
    }

    public void setDate(LocalDateTime date) throws RemoteException {
        this.date = date;
    }

//    @Override
//    public boolean equals(Object other) {
//        if (!(other instanceof ConcertServant))
//            return false;
//        if (other == this)
//            return true;
//
//        ConcertServant rhs = (ConcertServant)  other;
//        try {
//            return new EqualsBuilder().
//                    append(id, rhs.getId()).
//                    append(title, rhs.getTitle()).
//
//                    isEquals();
//        } catch (RemoteException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
//
//    @Override
//    public int hashCode() {
//        return new HashCodeBuilder(17, 31).
//                append(getClass().getName()).
//                append(id).
//                append(title).
//                toHashCode();
//    }
}

