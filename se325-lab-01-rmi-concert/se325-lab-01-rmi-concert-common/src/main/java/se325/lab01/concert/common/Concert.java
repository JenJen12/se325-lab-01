package se325.lab01.concert.common;


import java.time.LocalDateTime;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Concert extends Remote{


    Long getId() throws RemoteException ;

    void setId(Long id) throws RemoteException;

    String getTitle() throws RemoteException;

    void setTitle(String title) throws RemoteException;

    LocalDateTime getDate() throws RemoteException;

    void setDate(LocalDateTime date) throws RemoteException;

//
//    boolean equals(Object other) throws RemoteException;
//
//
//    int hashCode() throws RemoteException;
}
